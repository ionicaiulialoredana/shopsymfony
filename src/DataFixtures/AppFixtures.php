<?php

namespace App\DataFixtures;
use App\Entity\User;
use App\Entity\Product;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use DateTime;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
       $this->loadProducts($manager);
        $this->loadUser($manager);
    }

    public function loadProducts(ObjectManager $manager)
    {
        for($i=0; $i<34;$i++) {
            $product = new Product();
            $product->setName('Product ' . rand(1, 100));
            $product->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                Quisque sit amet euismod velit, sit amet mollis dui. Nullam a ipsum et ipsum laoreet pharetra. 
                Suspendisse ultricies sem vitae ligula elementum pretium. Lorem ipsum dolor sit amet,  Maecenas maximus sem a orci 
                ultrices, eget auctor enim condimentum. Phasellus pellentesque justo in viverra tristique. 
                Aliquam id ipsum a est sodales luctus et eget enim. Aenean nec blandit sem. ');
            $product->setPrice(rand(300, 1700));
            $product->setPhoto('pic'.rand(1, 14).'.jpeg');
            $manager->persist($product);
        }
        $manager->flush();
    }

    public function loadUser(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('admin');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'admin'));
        $user->setRoles(array('roles' =>'ROLE_ADMIN'));
        $user->setUsername('admin');
        $manager->persist($user);

        //ad more users
        $manager->flush();
    }


}
