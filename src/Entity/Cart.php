<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 */
class Cart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="carts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


    /**
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private $status = 'new';


    /**
     * @var CartItem[]
     *
     * @ORM\OneToMany(targetEntity="CartItem", mappedBy="cart")
     * */
    private $cartItems;


    /**
     * Cart constructor.
     * @param CartItem[] $cartItems
     */
    public function __construct()
    {
        $this->cartItems = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Cart
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Cart
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     *
     */
    public function getCartItems()
    {
        return $this->cartItems;
    }

    /**
     * @param CartItem[] $cartItems
     * @return Cart
     */
    public function setCartItems($cartItems)
    {
        $this->cartItems = $cartItems;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Cart
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }


}
