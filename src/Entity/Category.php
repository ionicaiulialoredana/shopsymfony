<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Tools\Console\Command\EnsureProductionSettingsCommand;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name='';

    /**
     * @var  Product[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="category")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="id", referencedColumnName="categoryId")
     * })
     */
    private $products;

    function __toString()
    {
        return $this->name;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     * @return Category
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }



}
