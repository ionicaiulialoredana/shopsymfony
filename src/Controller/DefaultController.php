<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Category;
use App\Entity\Product;
use App\Services\ShoppingCart;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(ShoppingCart $shoppingCart)
    {
        $entityManager=$this->getDoctrine()->getManager();
        $categories=$entityManager->getRepository(Category::class)->findAll();
        $products=$entityManager->getRepository(Product::class)->findBy([], null,6);
        $newArrivals=$entityManager->getRepository(Product::class)->findBy([],['id'=>'DESC'],6);
        $all=$entityManager->getRepository(Product::class)->findAll();
        return $this->render('default/index.html.twig',['shoppingCart'=>$shoppingCart,'categories'=>$categories,'newArrivals'=>$newArrivals, 'products'=>$products, 'threeItems'=>$all]);
    }

    /**
     * @Route("/addToCart/{id}", name="addToCart")
     */
    public function addToCart($id, ShoppingCart $shoppingCart, Request $request)
    {
        $entityManager=$this->getDoctrine()->getManager();
        $product=$entityManager->getRepository(Product::class)->find($id);
        $shoppingCart->addToCart($product,$request->query->get('q', 1));

        return $this->redirectToRoute('homepage');
    }


    /**
     * @Route("/updateCart/{id}", name="updateCart")
     */
    public function updateCart(ShoppingCart $shoppingCart,Request $request, $id)
    {
        $entityManager=$this->getDoctrine()->getManager();
        $cartItem = $entityManager->getRepository(CartItem::class)->find($id);
        $shoppingCart->updateCart($cartItem->getProduct(), $request->query->get('q'));

        return $this->redirectToRoute('showCart');
    }

      /**
     * @Route("/clearCart", name="clearCart")
     */
    public function clearCart(ShoppingCart $shoppingCart)
    {
        $shoppingCart->clearCart();
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/showCart", name="showCart")
     */
    public function showCart(ShoppingCart $shoppingCart)
    {
        $em=$this->getDoctrine()->getManager();
        $categories=$em->getRepository(Category::class)->findAll();
        $showMyCart=$shoppingCart->getCart();
        return $this->render('default/cart.html.twig',['showMyCart'=>$showMyCart,'shoppingCart'=>$shoppingCart,'categories'=>$categories]);
    }


    /**
     * @Route("/product/{id}", name="product-action")
     */
    public function product(ShoppingCart $shoppingCart,$id)
    {
        $em=$this->getDoctrine()->getManager();
        $categories=$em->getRepository(Category::class)->findAll();
        $product=$em->getRepository(Product::class)->find($id);
        return $this->render('default/shop-item.html.twig',['shoppingCart'=>$shoppingCart,'categories'=>$categories, 'product'=>$product]);

    }
    /**
     * @Route("/category/{id}", name="category-action")
     */
    public function category(ShoppingCart $shoppingCart,$id)
    {
        $em=$this->getDoctrine()->getManager();
        $categories=$em->getRepository(Category::class)->findAll();
        $mainCategory=$em->getRepository(Category::class)->find($id);
        return $this->render('default/shop-product-list.html.twig',['shoppingCart'=>$shoppingCart,'categories'=>$categories, 'mainCategory'=>$mainCategory]);

    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/checkoutCart", name="checkoutCart")
     */
    public function checkoutCart(ShoppingCart $shoppingCart, Request $request)
    {
        $shoppingCart->checkoutCart();
        return $this->redirectToRoute('homepage');
    }
}
